/*
  Service utilisé pour conserver les données de l'application entre les pages
*/
import { Injectable } from '@angular/core';
import { Player } from './classes/player';
import { Game } from './classes/game';
import { GameContent } from './classes/model/gamecontent';

@Injectable({
  providedIn: 'root'
})
export class DatastoreService {

  // Métadonnées du joueur
  private player: Player;

  // Métadonnées de la partie
  private game: Game;

  // Données actives de la partie
  private gamecontent: GameContent;

  constructor() { }

  public setGame(game_data)
  {
    this.game = game_data;
  }

  public getGame(): Game
  {
    return this.game;
  }

  public setPlayer(player_data)
  {
    this.player = player_data;
  }

  public getPlayer(): Player
  {
    return this.player;
  }

  public getGameContent(): GameContent
  {
    return this.gamecontent;
  }

  public setGameContent(gamecontent_data)
  {
    this.gamecontent = gamecontent_data;
  }
}
