/*
  Pipe utilisée pour savoir si une variable est présente dans un tableau
*/
import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'arrayContains'
})
export class ArrayContainsPipe implements PipeTransform {

  transform(value: any[], find: any): boolean {
    let res = false;
    value.forEach(function(x){
      if(x==find){
        res = true;
      }
    });
    return res;
  }
}
