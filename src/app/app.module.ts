import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { LobbyComponent } from './lobby/lobby.component';
import { GameCreatorComponent } from './game-creator/game-creator.component';
import { HeroSelectionComponent } from './hero-selection/hero-selection.component';
import { GameJoinComponent } from './game-join/game-join.component';
import { GameBoardComponent } from './game-board/game-board.component';
import { ServantDetailsComponent } from './servant-details/servant-details.component';
import { SpellDetailsComponent } from './spell-details/spell-details.component';
import { TurnClockComponent } from './turn-clock/turn-clock.component';
import { ArrayContainsPipe } from './array-contains.pipe';

@NgModule({
  declarations: [
    AppComponent,
    LobbyComponent,
    GameCreatorComponent,
    HeroSelectionComponent,
    GameJoinComponent,
    GameBoardComponent,
    ServantDetailsComponent,
    SpellDetailsComponent,
    TurnClockComponent,
    ArrayContainsPipe,
  ],
  imports: [
    BrowserModule,
    FormsModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
