/*
  Composant représentant l'arrivée dans une partie créée
*/
import { Component, OnInit } from '@angular/core';
import { Player } from '../classes/player';
import { Game } from '../classes/game';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import { Observable, of } from 'rxjs';
import { switchMap } from 'rxjs/operators';
import { WebsocketService } from '../websocket.service';
import { DatastoreService } from '../datastore.service';

@Component({
  selector: 'app-game-join',
  templateUrl: './game-join.component.html',
  styleUrls: ['./game-join.component.css']
})
export class GameJoinComponent implements OnInit {

  player : Player = {
    name: '',
    id: null,
    key: ''
  }

  game: Game = {
    id: null,
    key: ''
  }

  subscription: any;

  constructor(
    private websocket : WebsocketService,
    private datastore : DatastoreService,
    private router :  Router,
    private route: ActivatedRoute
  )
  {
      // Recupère l'id et la clef de la partie dans l'url si présents
      this.game = {
        id: +this.route.snapshot.paramMap.get("id"),
        key: this.route.snapshot.paramMap.get("key")
      }
  }

  ngOnInit() {
  }

  ngOnDestroy(){
      this.subscription.unsubscribe();
  }

  onClickJoin()
  {
    if(this.player.name && this.player.key && this.game.key && this.game.id)
    {
      this.websocket.joinGame(this.player,this.game);
      this.websocket.startGameSubscription(this.game);
      this.subscription = this.websocket.getGameSubscription().subscribe(content => this.handleGameContent(JSON.parse(content.body)));
    }
  }

  handleGameContent(gameContent)
  {
    this.player.id = gameContent.player2Id;
    console.log(gameContent.myId);
    console.log(this.player);
    this.datastore.setGame(this.game);
    this.datastore.setPlayer(this.player);
    this.router.navigate(["/hero"]);
  }
}
