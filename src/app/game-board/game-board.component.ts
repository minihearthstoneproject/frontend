/*
  Composant pour représenter une partie en cours
*/
import { Component, OnInit, EventEmitter, Pipe, PipeTransform } from '@angular/core';
import { Game } from '../classes/game';
import { Player } from '../classes/player';
import { DatastoreService } from '../datastore.service';
import { WebsocketService } from '../websocket.service';
import { GameContent } from '../classes/model/gamecontent';
import { Card } from '../classes/model/card';
import { Hero } from '../classes/model/hero';
import { Servant } from '../classes/model/servant';
import { Spell } from '../classes/model/spell';
import { SpecialAction,Provocation } from '../classes/model/specialact';

@Component({
  selector: 'app-game-board',
  templateUrl: './game-board.component.html',
  styleUrls: ['./game-board.component.css']
})
export class GameBoardComponent implements OnInit {

  // Données actives de la partie
  gamecontent: GameContent;
  hand: Card[];
  myHero: Hero;
  oppositeHero: Hero;
  myPlayingCards : Servant[];
  oppositePlayingCards : Servant[];
  myMana: number;
  oppositeMana: number;

  // Variables utiles pour gérer les selections de cibles et sources d'actions
  selection1Needed: boolean;
  selection1: any;
  selection1Id: number;
  selection2: any;
  selection2Id: number;
  selection2Needed: boolean;
  servantClicked: EventEmitter<any> = new EventEmitter();
  heroClicked: EventEmitter<any> = new EventEmitter();
  servantClickSubscription: any;
  heroClickSubscription: any;

  // Message indiquant qui joue
  turnMessage: string;

  // Variables utiles pour la gestion locale des effets de l'action spéciale Provocation
  provocked: boolean;
  provockingServantsIds: number[];

  // Variables utiles pour la génération de notifications
  warning: boolean;
  warningMessage: string;

  // Indique si le joueur peut jouer
  myTurn: boolean;

  constructor(
    private datastore : DatastoreService,
    private websocket : WebsocketService
  ) { }

  // Lorsque le composant est initialisé, l'objet GameContent est recuperé dans le service Datastore et les variables du composant sont renseignées
  ngOnInit() {
    this.gamecontent = this.datastore.getGameContent();
    this.feedGameContent();
    this.websocket.getGameSubscription().subscribe(content => this.handleGameContent(JSON.parse(content.body)));
    console.log("GAME CONTENT:",this.gamecontent);
  }

  // Renseigne les variables du composant en fonction de l'objet GameContent
  feedGameContent()
  {
    this.getHand();
    if(this.gamecontent.player1Id == this.datastore.getPlayer().id)
    {
      this.myHero = this.gamecontent.hero1;
      this.oppositeHero = this.gamecontent.hero2;
      this.myPlayingCards = this.gamecontent.playingCards1;
      this.oppositePlayingCards = this.gamecontent.playingCards2;
      this.myMana = this.gamecontent.mana1;
      this.oppositeMana = this.gamecontent.mana2;
      this.turnMessage = (this.gamecontent.turn == 1) ? "C'est votre tour!" : "C'est au tour de l'adversaire!";
      this.myTurn = (this.gamecontent.turn == 1);
    }else{
      this.myHero = this.gamecontent.hero2;
      this.oppositeHero = this.gamecontent.hero1;
      this.myPlayingCards = this.gamecontent.playingCards2;
      this.oppositePlayingCards = this.gamecontent.playingCards1;
      this.myMana = this.gamecontent.mana2;
      this.oppositeMana = this.gamecontent.mana1;
      this.turnMessage = (this.gamecontent.turn == 2) ? "C'est votre tour!" : "C'est au tour de l'adversaire!";
      this.myTurn = (this.gamecontent.turn == 2);
    }
    this.provockingServantsIds = new Array<number>();
    let that = this;
    this.oppositePlayingCards.forEach(function(servant){
      if(servant.specialaction!=null)
      {
        if(servant.specialaction.name == new Provocation().name)
        {
          that.provockingServantsIds.push(servant.id);
          that.provocked = true;
        }
      }
    })
  }

  // Active une notification avec le message contenu dans le paramètre msg
  enableWarning(msg: string)
  {
    this.warning = true;
    this.warningMessage = msg;
    let that = this;
    setTimeout(function(){that.disableWarning();},7000);
  }

  disableWarning()
  {
    this.warning = false;
    this.warningMessage = "";
  }

  // Recupère la main du joueur
  getHand()
  {
    this.websocket.getHand(this.datastore.getPlayer(),this.datastore.getGame()).subscribe(content => this.saveHand(JSON.parse(content.body)));
  }

  // Sauvegarde la main du joueur dans la variable hand du composant
  saveHand(data)
  {
    this.hand = data.hand.map(str => this.gamecontent.buildCardFromString(str));
    console.log("Hand: "+this.hand[0].name);
  }

  // Gère l'arrivée de nouvelles données sur la partie en provenance du serveur
  handleGameContent(gameContent)
  {
    this.gamecontent = new GameContent(gameContent);
    this.datastore.setGameContent(this.gamecontent);
    this.feedGameContent();
  }

  onClickSpecialAction()
  {
    if(this.myTurn)
    {
      if(this.myMana>=2)
      {
        if(this.myHero.canAttack)
        {
          if(this.myHero.specialaction.targetedHeroAction)
          {
            this.selection2Needed = true;
            this.resetHeroClickSubscription();
            this.heroClickSubscription = this.heroClicked.subscribe(next => {this.callAction(this.myHero.id,this.selection2.id);});
            this.resetServantClickSubscription();
            this.servantClickSubscription = this.servantClicked.subscribe(next =>{this.callAction(this.myHero.id,this.selection2.id);});
          }else{
            this.callAction(this.myHero.id,null);
          }
        }else{
          this.enableWarning("Votre héros a déjà utilisé son action spéciale!");
        }
      }else{
        this.enableWarning("Vous n'avez pas assez de mana!");
      }
    }else{
      this.enableWarning("Ce n'est pas à vous de jouer!");
    }
  }


  onClickOppositeHero()
  {
    console.log("opposite hero clicked");
    if(this.selection2Needed)
    {
      console.log("opposite hero clicked and needed")
      this.setSelection2(this.oppositeHero);
      this.selection2Needed = false;
      this.heroClicked.emit();
    }
  }

  onClickServant(servant: Servant)
  {
    console.log("Servant clicked")
    if(this.selection1Needed)
    {
      this.setSelection1(servant);
      this.selection1Needed = false;
      this.servantClicked.emit();
    }
    else if(this.selection2Needed)
    {
      this.setSelection2(servant);
      this.selection2Needed = false;
      this.servantClicked.emit();
    }
  }

  setSelection1(entity: any)
  {
      this.selection1 = entity;
      this.selection1Id = entity.id;
  }

  setSelection2(entity: any)
  {
      this.selection2 = entity;
      this.selection2Id = entity.id;
  }

  resetSelections()
  {
    this.selection1 = null;
    this.selection2 = null;
    this.selection1Id = null;
    this.selection2Id = null;
  }

  resetServantClickSubscription()
  {
    if(this.servantClickSubscription!=null)
    {
      this.servantClickSubscription.unsubscribe();
    }
  }

  resetHeroClickSubscription()
  {
    if(this.heroClickSubscription!=null)
    {
      this.heroClickSubscription.unsubscribe();
    }
  }

  onClickAttack()
  {
    if(this.myTurn)
    {
      if(this.oppositePlayingCards.length == 0)
      {
        this.selection1Needed = true;
        this.resetServantClickSubscription()
        this.servantClickSubscription = this.servantClicked.subscribe(next => {this.callAction(this.selection1.id,this.oppositeHero.id)});
      }else{
        this.selection1Needed = true;
        this.resetServantClickSubscription()
        this.servantClickSubscription = this.servantClicked.subscribe(next => {this.waitForAttackTarget()});
      }
    }else{
      this.enableWarning("Ce n'est pas à vous de jouer!");
    }
  }

  waitForAttackTarget()
  {
      this.selection2Needed = true;
      this.resetServantClickSubscription()
      this.servantClickSubscription = this.servantClicked.subscribe(next => {
        if(this.provocked && !this.provockingServantsIds.includes(this.selection2.id)){
          this.enableWarning("Un ou plusieurs serviteurs adverses vous provoquent, vous ne pouvez pas attaquer les autres.")
        }else{
          if(this.selection1.canAttack)
          {
            this.callAction(this.selection1.id,this.selection2.id)
          }else{
            this.resetSelections();
            this.enableWarning("Ce serviteur ne peut pas attaquer.");
          }
        }
      });
  }

  callAction(sourceId: number,targetId: number)
  {
    this.resetSelections();
    console.log("action called");

    this.websocket.action(this.datastore.getPlayer(),this.datastore.getGame(),sourceId,targetId);
  }

  onClickInvoke(card: Card)
  {
    console.log("nom à invoquer: ",card.name);
    if(this.myTurn)
    {
      if(card.type === "spell" && (card as Spell).needsTarget)
      {
        if((card as Spell).cost <= this.myMana)
        {
          this.selection2Needed = true;
          this.resetServantClickSubscription()
          this.servantClickSubscription = this.servantClicked.subscribe(next => {this.callInvokeCard(card,this.selection2.id)});
        }else{
          this.enableWarning("Vous n'avez pas assez de mana!");
        }
      }else{
        if((card as Servant).cost <= this.myMana)
        {
          this.callInvokeCard(card,null);
        }else{
          this.enableWarning("Vous n'avez pas assez de mana!");
        }
      }
    }else{
      this.enableWarning("Ce n'est pas à vous de jouer!");
    }
  }

  callInvokeCard(card: Card,targetId: number)
  {
    this.resetSelections();
    this.websocket.invokeCard(this.datastore.getPlayer(),this.datastore.getGame(),card,targetId);
  }

  onClickDetails(card: Card)
  {

  }

  onClickCheck()
  {
    if(this.myTurn)
    {
      this.websocket.check(this.datastore.getPlayer(),this.datastore.getGame());
    }else{
      this.enableWarning("Ce n'est pas à vous de jouer!");
    }
  }

  onClickForfeit()
  {
    if(this.myTurn)
    {
      this.websocket.forfeit(this.datastore.getPlayer(),this.datastore.getGame());
    }else{
      this.enableWarning("Ce n'est pas à vous de jouer!");
    }
  }


}
