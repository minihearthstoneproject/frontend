// Composant utile pour l'affichage des caractéristiques d'un serviteur
import { Component, OnInit,Input } from '@angular/core';
import { Card } from '../classes/model/card';
import { Servant } from '../classes/model/servant';

@Component({
  selector: 'app-servant-details',
  templateUrl: './servant-details.component.html',
  styleUrls: ['./servant-details.component.css']
})
export class ServantDetailsComponent implements OnInit {

  @Input() card: Card;
  servant: Servant;

  constructor() { }

  ngOnInit() {
    this.servant = this.card as Servant;
  }

}
