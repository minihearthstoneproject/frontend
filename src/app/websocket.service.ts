/*
  Service utilisé pour communiquer avec le serveur via websocket
*/
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { fromEvent } from 'rxjs';
import {environment} from '../environments/environment';
import { Game } from './classes/game';
import { Player } from './classes/player';
import { Card } from './classes/model/card';
import { Servant } from './classes/model/servant';

declare var require: any;
const Stomp = require("stompjs/lib/stomp.js").Stomp;

@Injectable({
  providedIn: 'root'
})
export class WebsocketService {

  private stompClient;

  private creationSubscription;

  private gameSubscription;

  constructor() { }

  // Connexion au websocket du serveur
  public connect(): void{
    this.stompClient = Stomp.client(environment.websocketRegisterUrl);
    this.stompClient.connect({},function(frame){
      // Callback for connection
    });
  }

  // Inscription au canal websocket de la partie passée en paramètre
  public startGameSubscription(game) {
    let that = this;
    let obs =
    Observable.create(function(observer){
      that.stompClient.subscribe("/game/"+game.id,function(data){
        observer.next(data);
      });
    });

    this.gameSubscription = obs;
  }

  // Recupération de l'observateur sur le canal websocket de la partie, permet de savoir si un message arrive sur le canal
  public getGameSubscription(): Observable<any>
  {
    return this.gameSubscription;
  }

  // Appel au serveur pour créer une partie
  public createGame(player: Player,game: Game): Observable<any>{
    let that = this;
    let obs =
    Observable.create(function(observer){
      this.creationSubscription =
      that.stompClient.subscribe('/com/createGame/'+game.key+"/"+player.name+"/"+player.key,
      function(data){
        observer.next(data);
      });
    });

    return obs;
  }

  // Appel au serveur pour lancer une action (action spéciale, attaque)
  public action(player: Player,game: Game,sourceId: number,targetId: number)
  {
      let vars = {
        gameId: game.id,
        gameKey: game.key,
        playerId: player.id,
        playerKey: player.key,
        sourceId: sourceId,
        targetId: targetId
      }
      this.stompClient.send("/com/action/"+game.id,{},JSON.stringify(vars));
  }

  // Appel au serveur pour invoquer une carte (sort,serviteur)
  public invokeCard(player: Player,game: Game,card: Card,targetId)
  {
    let vars = {
      gameId: game.id,
      gameKey: game.key,
      playerId: player.id,
      playerKey: player.key,
      cardName: card.name,
      targetId: targetId
    }
    this.stompClient.send("/com/invokeCard/"+game.id,{},JSON.stringify(vars));
  }

  // Appel au serveur pour passer son tour
  public check(player: Player,game: Game)
  {
    let vars = {
      gameId: game.id,
      gameKey: game.key,
      playerId: player.id,
      playerKey: player.key,
    }
    this.stompClient.send("/com/check/"+game.id,{},JSON.stringify(vars));
  }

  // Appel au serveur pour abandonner la partie
  public forfeit(player: Player,game: Game)
  {
    let vars = {
      gameId: game.id,
      gameKey: game.key,
      playerId: player.id,
      playerKey: player.key,
    }
    this.stompClient.send("/com/forfeit/"+game.id,{},JSON.stringify(vars));
  }

  // Appel au serveur pour assigner un héros
  public setHero(player: Player,game: Game ,hero){
    let vars = {
      gameId: game.id,
      gameKey: game.key,
      playerId: player.id,
      playerKey: player.key,
      hero: hero
    }
    this.stompClient.send("/com/setHero/"+game.id,{},JSON.stringify(vars));
  }

  // Appel au serveur pour recuperer la main d'un joueur
  public getHand(player: Player,game: Game) : Observable<any>
  {
      let that = this;
      let obs =
      Observable.create(function(observer){
        that.stompClient.subscribe('/com/getHand/'+game.id+"/"+game.key+"/"+player.id+"/"+player.key,
        function(data){
          observer.next(data);
        });
      });

      return obs;
  }

  // Appel au serveur pour rejoindre une partie
  public joinGame(player: Player,game: Game)
  {
    let vars = {
      username: player.name,
      gameKey: game.key,
      userKey: player.key
    }
    this.stompClient.send("/com/joinGame/"+game.id,{},JSON.stringify(vars));
  }

  public cancelCreationSubscription()
  {
    this.creationSubscription.unsubscribe();
  }

}
