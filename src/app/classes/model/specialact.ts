//Représente une action spéciale d'un héros ou l'effet d'une carte ( sort ou serviteur )
export class SpecialAction
{
  name: string;
  description: string;
  targetedHeroAction: boolean;

  constructor(name,description)
  {
    this.name = name;
    this.description = description;
    this.targetedHeroAction = false;
  }
}

export class SAMage extends SpecialAction
{
  constructor()
  {
    super("Boule de feu","Inflige 1 point de dégât à un serviteur ou à un héros");
    this.targetedHeroAction = true;
  }
}

export class SAPaladin extends SpecialAction
{
  constructor()
  {
    super("Renfort","Invoque un serviteur: Recrue de la main d'argent");
  }
}

export class SAGuerrier extends SpecialAction
{
  constructor()
  {
    super("Armure","Ajoute 2 points d'armure à votre héros");
  }
}

export class Provocation extends SpecialAction
{
  constructor()
  {
    super("Provocation","votre adversaire est obligé d'attaquer ce serviteur avant d'attaquer votre héros ou un autre serviteur sans provocation");
  }

}

export class Charge extends SpecialAction
{
  constructor()
  {
    super("Charge","Le serviteur peut attaquer dès qu'il est posé sur le plateau, sans attendre le tour suivant")
  }
}

export class VolDeVie extends SpecialAction
{
  constructor()
  {
    super("Vol de vie","Lorsque le serviteur inflige des dégats, votre héros recupère des points de vie à la hauteur des dégats infligés")
  }
}

export class SAChefRaid extends SpecialAction
{
  constructor()
  {
    super("Effet du chef de raid","+1 aux dégats de tout les serviteurs alliés du plateau");
  }
}

export class SAChampionFrisselame extends SpecialAction
{
  constructor()
  {
    super("Charge et Vol de vie","Le serviteur peut attaquer dès qu'il est posé sur le plateau, sans attendre le tour suivant ET Lorsque le serviteur inflige des dégats, votre héros recupère des points de vie à la hauteur des dégats infligés")
  }
}

export class SAImageMiroir extends SpecialAction
{
  constructor()
  {
    super("Effet d'image miroir","Invoque deux serviteurs 0/2 avec provocation");
  }
}

export class SAExplosionArcanes extends SpecialAction
{
  constructor()
  {
    super("Effet d'explosion des arcanes","Inflige 1 point de dégât à tout les serviteurs adverses");
  }
}

export class SAMetamorphose extends SpecialAction
{
  constructor()
  {
    super("Effet de métamorphose","Transforme un serviteur en serviteur 1/1 sans effet special");
  }
}

export class SATourbillon extends SpecialAction
{
  constructor()
  {
    super("Effet de tourbillon","Inflige 1 point de dégât à tout les serviteurs");
  }
}

export class SAMaitriseBlocage extends SpecialAction
{
  constructor()
  {
    super("Effet de maitrise du blocage","+5 armure et place une carte aléatoire de la pioche dans votre main");
  }
}

export class SABenedictionPuissance extends SpecialAction
{
  constructor()
  {
    super("Effet de bénédiction de puissance","confère +3 d'attaque à un serviteur");
  }
}

export class SAConsecration extends SpecialAction
{
  constructor()
  {
    super("Effet de consécration","Inflige 2 points de dégâts à tous les adversaires")
  }
}
