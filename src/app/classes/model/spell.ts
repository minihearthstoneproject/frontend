// Représente un sort, chaque sort existant est représenté par une sous-classe de Spell
import { Card } from './card';
import * as SAMeta from './specialact';

export class Spell extends Card
{

  specialaction: any;
  cost: number;
  needsTarget: boolean;

  constructor(name,specialaction,cost,needsTarget)
  {
    super(name,"spell");
    this.specialaction = specialaction;
    this.cost = cost;
    this.needsTarget = needsTarget;
  }
}

export class ImageMiroir extends Spell
{
  constructor(){
    super("image miroir",new SAMeta.SAImageMiroir(),1,false);
  }
}

export class ExplosionArcanes extends Spell
{
  constructor(){
    super("explosion des arcanes",new SAMeta.SAExplosionArcanes(),2,false);
  }
}

export class Metamorphose extends Spell
{
  constructor(){
    super("metamorphose",new SAMeta.SAMetamorphose(),4,true);
  }
}

export class Tourbillon extends Spell
{
  constructor(){
    super("tourbillon",new SAMeta.SATourbillon(),1,false);
  }
}

export class MaitriseBlocage extends Spell
{
  constructor(){
    super("maitrise du blocage",new SAMeta.SAMaitriseBlocage(),3,false);
  }
}

export class BenedictionPuissance extends Spell
{
  constructor()
  {
    super("benediction de puissance",new SAMeta.SABenedictionPuissance(),1,true);
  }
}

export class Consecration extends Spell
{
  constructor()
  {
    super("consecration",new SAMeta.SAConsecration(),4,false);
  }
}
