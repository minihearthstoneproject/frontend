/*
  Représente une carte (serviteur ou sort), superclasse pour Spell et Servant
*/
export abstract class Card
{
  type: string;
  name: string;

  constructor(name,type)
  {
    this.name = name;
    this.type = type;
  }


}
