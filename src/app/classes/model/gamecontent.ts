/*
  Classe contenant les informations communes aux deux joueurs sur la partie en cours
*/
import { Servant } from './servant';
import { Card } from './card';
import * as Servants from './servant';
import * as Spells from './spell';
import { Hero } from './hero';
import * as Heroes from './hero';

export class GameContent {

  // Carte en jeu du joueur 1
  playingCards1: Servant[];

  // Carte en jeu du joueur 2
  playingCards2: Servant[];

  // Héros du joueur 1
  hero1: Hero;

  // Héros du joueur 2
  hero2: Hero;

  // Mana du joueur 1
  mana1: number;

  // Mana du joueur 2
  mana2: number;

  // Designe le joueur dont le tour est en cours (1 pour joueur 1, 2 pour joueur 2)
  turn: number;

  // Indique si la partie est lancée (les 2 joueurs ont selectionné leur héros)
  active: boolean;

  // Indique si deux joueurs sont inscrits dans la partie
  allPlayersConnected: boolean;

  // Indique l'id du joueur ayant provoqué l'envoi de ces nouvelles informations sur la partie en cours
  callerId: number;

  // Id du joueur 1
  player1Id: number;

  // Id du joueur 2
  player2Id: number;

  // Timestamp pour la date de départ du tour en cours
  timerStart: number;

  constructor(data: any)
  {
    if(data.playingCards1!=null)
    {
      this.playingCards1 =
        data.playingCards1.map(obj => this.buildServantFromObject(obj));
    }

    if(data.playingCards2!=null)
    {
      this.playingCards2 =
        data.playingCards2.map(obj => this.buildServantFromObject(obj));
    }

    this.hero1 = this.buildHeroFromObject(data.hero1);
    this.hero2 = this.buildHeroFromObject(data.hero2);
    this.mana1 = data.mana1;
    this.mana2 = data.mana2;
    this.turn = data.turn;
    this.active = data.active;
    this.allPlayersConnected = data.allPlayersConnected;
    this.callerId = data.callerId;
    this.player1Id = data.player1Id;
    this.player2Id = data.player2Id;
    this.timerStart = data.timerStart;
  }

  // Construire un objet Card à partir d'une châine de caractère, sert à construire la main des joueurs à partir des noms de carte renvoyés par le serveur
  buildCardFromString(str: string) : Card
  {
    switch(str)
    {
      default:
        return new Servants.SanglierBrocheroc(null,1,false);
      break;

      case "sanglier brocheroc":
        return new Servants.SanglierBrocheroc(null,1,false);
      break;

      case "soldat du compte de l or":
        return new Servants.SoldatComteOr(null,2,false);
      break;

      case "chevaucheur de loup":
        return new Servants.ChevaucheurLoup(null,1,false);
      break;

      case "chef de raid":
        return new Servants.ChefRaid(null,2,false);
      break;

      case "yeti noroit":
        return new Servants.YetiNoroit(null,5,false);
      break;

      case "champion frisselame":
        return new Servants.ChampionFrisselame(null,2,false);
      break;

      case "avocat commis d office":
        return new Servants.AvocatCommisOffice(null,7,false);
      break;

      case "recrue de la main d argent":
        return new Servants.RecrueMainArgent(null,1,false);
      break;

      case "image miroir servant":
        return new Servants.MiroirServant(null,2,false);
      break;

      case "image miroir":
        return new Spells.ImageMiroir();
      break;

      case "explosion des arcanes":
        return new Spells.ExplosionArcanes();
      break;

      case "metamorphose":
        return new Spells.Metamorphose();
      break;

      case "tourbillon":
        return new Spells.Tourbillon();
      break;

      case "maitrise du blocage":
        return new Spells.MaitriseBlocage();
      break;

      case "benediction de puissance":
        return new Spells.BenedictionPuissance();
      break;

      case "consecration":
        return new Spells.Consecration();
      break;
    }
  }

  // Reconstruit un objet Servant à partir d'un objet Servant venant du serveur
  buildServantFromObject(obj: any) : Servant
  {
    let servant = this.buildCardFromString(obj.name) as Servant;
    servant.id = obj.id;
    servant.healthPoint = obj.healthPoint;
    servant.canAttack = obj.canAttack;
    servant.damages = obj.attack;
    return servant;
  }

  // Construit un objet Hero à partir d'un objet Hero venant du serveur
  buildHeroFromObject(obj: any) : Hero
  {
    let hero;
    switch(obj.name)
    {
      case "guerrier":
        hero = new Heroes.Guerrier();
        hero.id = obj.id;
        hero.armor = obj.armorLeft;
        hero.hp = obj.hpLeft;
        hero.canAttack = obj.canAttack;
        return hero;
      break;

      case "mage":
        hero = new Heroes.Mage();
        hero.id = obj.id;
        hero.armor = obj.armorLeft;
        hero.hp = obj.hpLeft;
        hero.canAttack = obj.canAttack;
        return hero;
      break;

      case "paladin":
        hero = new Heroes.Paladin();
        hero.id = obj.id;
        hero.armor = obj.armorLeft;
        hero.hp = obj.hpLeft;
        hero.canAttack = obj.canAttack;
        return hero;
    }
  }
}
