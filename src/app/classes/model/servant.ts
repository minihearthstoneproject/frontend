// Représente un serviteur, chaque serviteur existant est representé par une sous-classe de Servant
import * as SAMeta from './specialact';
import { Card } from './card';

export class Servant extends Card{
    id: number;
    healthPoint: number;
    canAttack: boolean;
    specialaction: any;
    damages: number;
    cost: number;


    constructor(id,name,healthPoint,canAttack,specialaction,damages,cost)
    {
      super(name,"servant");
      this.id = id;
      this.healthPoint = healthPoint;
      this.canAttack = canAttack;
      this.specialaction = specialaction;
      this.damages = damages;
      this.cost = cost;
    }

}

export class SanglierBrocheroc extends Servant
{
  constructor(id,healthPoint,canAttack)
  {
    super(id,"sanglier brocheroc",healthPoint,canAttack,null,1,1);
  }
}

export class SoldatComteOr extends Servant
{
  constructor(id,healthPoint,canAttack)
  {
    super(id,"soldat du compte de l or",healthPoint,canAttack,new SAMeta.Provocation(),1,1);
  }
}

export class ChevaucheurLoup extends Servant
{
  constructor(id,healthPoint,canAttack)
  {
    super(id,"chevaucheur de loup",healthPoint,canAttack,new SAMeta.Charge(),3,3);
  }
}

export class ChefRaid extends Servant
{
  constructor(id,healthPoint,canAttack)
  {
    super(id,"chef de raid",healthPoint,canAttack,new SAMeta.SAChefRaid(),2,3);
  }
}

export class YetiNoroit extends Servant
{
  constructor(id,healthPoint,canAttack)
  {
    super(id,"yeti noroit",healthPoint,canAttack,null,4,4);
  }
}

export class ChampionFrisselame extends Servant
{
  constructor(id,healthPoint,canAttack)
  {
    super(id,"champion frisselame",healthPoint,canAttack,new SAMeta.SAChampionFrisselame(),3,4);
  }
}

export class AvocatCommisOffice extends Servant
{
  constructor(id,healthPoint,canAttack)
  {
    super(id,"avocat commis d office",healthPoint,canAttack,new SAMeta.Provocation(),0,2);
  }
}

export class RecrueMainArgent extends Servant
{
  constructor(id,healthPoint,canAttack)
  {
    super(id,"recrue de la main d argent",healthPoint,canAttack,null,1,0);
  }
}

export class MiroirServant extends Servant
{
  constructor(id,healthPoint,canAttack)
  {
    super(id,"image miroir servant",healthPoint,canAttack,new SAMeta.Provocation(),0,0);
  }
}
