// Représente un héros
import * as SAMeta from './specialact';

export class Hero {
  id: number;
  name: string;
  hp: number;
  armor:number;
  specialaction:any;
  canAttack: boolean;

  constructor(name: string,specialaction: any){
    this.hp = 30;
    this.armor = 0;
    this.name = name;
    this.specialaction = specialaction;
    this.canAttack = false;
  }
}

export class Guerrier extends Hero
{
  constructor(){
    super("Guerrier",new SAMeta.SAGuerrier());
  }
}

export class Mage extends Hero
{
  constructor(){
    super("Mage",new SAMeta.SAMage());
  }
}

export class Paladin extends Hero
{
  constructor(){
    super("Paladin",new SAMeta.SAPaladin());
  }
}
