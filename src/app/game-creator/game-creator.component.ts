/*
  Composant pour représenter la création d'une partie
*/
import { Component, OnInit } from '@angular/core';
import { Player } from '../classes/player';
import { Game } from '../classes/game';
import { Router } from '@angular/router';
import {Observable, of} from 'rxjs';
import {WebsocketService} from '../websocket.service';
import {DatastoreService} from '../datastore.service';
import { UUID } from 'angular2-uuid';

@Component({
  selector: 'app-game-creator',
  templateUrl: './game-creator.component.html',
  styleUrls: ['./game-creator.component.css']
})
export class GameCreatorComponent implements OnInit {

  player: Player = {
    name: '',
    id: null,
    key: ''
  };

  game: Game = {
    id: null,
    key: UUID.UUID()
  }

  constructor(
    private router: Router,
    private websocket: WebsocketService,
    private datastore: DatastoreService
  ) { }

  ngOnInit() {

  }

  // Gestion de la réponse du serveur, les métadonnées de la partie sont transmises au service datastore
  handleCreationResponse(msg){
    let data = JSON.parse(msg.body);
    this.game.id = data.gameId;
    this.datastore.setGame(this.game);
    this.player.id = data.playerId;
    this.datastore.setPlayer(this.player);
    this.websocket.startGameSubscription(this.game);
    this.router.navigate(["/hero"]);
  }

  onClickCreate(){
    if(this.player.name && this.player.key)
    {
      // Appelle le serveur et attends la réponse
      this.websocket.createGame(this.player,this.game).subscribe((msg)=>{
        // Le serveur renvoie les informations sur la partie
        this.handleCreationResponse(msg);
      });
    }
  }

}
