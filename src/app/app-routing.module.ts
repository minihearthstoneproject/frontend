import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LobbyComponent } from './lobby/lobby.component';
import { GameCreatorComponent} from './game-creator/game-creator.component';
import { HeroSelectionComponent } from './hero-selection/hero-selection.component';
import { GameJoinComponent } from './game-join/game-join.component';
import { GameBoardComponent } from './game-board/game-board.component';

const routes: Routes = [
  { path: '',
    redirectTo: '/lobby',
    pathMatch: 'full'
  },

  { path: 'lobby',
    component: LobbyComponent
  },

  { path: 'creator',
    component: GameCreatorComponent
  },

  { path: 'hero',
    component: HeroSelectionComponent
  },

  { path: 'join',
    component: GameJoinComponent
  },

  { path: 'join/:id/:key',
    component: GameJoinComponent
  },

  {
    path: 'game',
    component: GameBoardComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [ RouterModule ]
})
export class AppRoutingModule { }
