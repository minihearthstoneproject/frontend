// Composant utile pour l'affichage des caractéristiques d'un sort
import { Component, OnInit,Input } from '@angular/core';
import { Card } from '../classes/model/card';
import { Spell } from '../classes/model/spell';

@Component({
  selector: 'app-spell-details',
  templateUrl: './spell-details.component.html',
  styleUrls: ['./spell-details.component.css']
})
export class SpellDetailsComponent implements OnInit {

  @Input() card: Card;
  spell: Spell;

  constructor() { }

  ngOnInit() {
    this.spell = this.card as Spell;
  }

}
