/*
  Composant représentant la selection d'un héros
*/
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import {Observable, of} from 'rxjs';
import {WebsocketService} from '../websocket.service';
import {DatastoreService} from '../datastore.service';
import {GameContent} from '../classes/model/gamecontent';
import {Game} from '../classes/game';
import {environment} from '../../environments/environment';

@Component({
  selector: 'app-hero-selection',
  templateUrl: './hero-selection.component.html',
  styleUrls: ['./hero-selection.component.css']
})
export class HeroSelectionComponent implements OnInit {

  // Choix de l'utilisateur
  herochoice: String;

  // Indique si il manque un joueur
  waitingForPlayers: boolean = false;

  // Indique si un joueur n'a pas selectionné son héros
  waitingForHeroSelection: boolean = false;

  game: Game;

  serverUrl : string;

  subscription: any;

  constructor(
    private router: Router,
    private websocket: WebsocketService,
    private datastore: DatastoreService
  )
  { }

  ngOnInit() {
    this.serverUrl = environment.serverUrl;
    this.game = this.datastore.getGame();
    this.subscription = this.websocket.getGameSubscription().subscribe(content => this.handleGameContent(JSON.parse(content.body)));
  }

  ngOnDestroy()
  {
    this.subscription.unsubscribe();
  }

  // Lorsque le bouton Valider le choix est cliqué la requête vers le serveur est lancée
  onHeroValidationClick()
  {
    if(this.herochoice)
    {
      this.websocket.setHero(this.datastore.getPlayer(),this.datastore.getGame(),this.herochoice);
    }
  }

  handleGameContent(gameContent)
  {
    if(!gameContent.allPlayersConnected)
    {
        this.waitingForPlayers = true;
    }
    else if(gameContent.allPlayersConnected && !gameContent.active)
    {
        this.waitingForPlayers = false;
        this.waitingForHeroSelection = true;
    }
    else
    {
        this.datastore.setGameContent(new GameContent(gameContent));
        this.router.navigate(["/game"]);
    }
  }

}
