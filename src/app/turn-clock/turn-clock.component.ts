// Composant utile pour représenter le temps restant dans un tour
import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-turn-clock',
  templateUrl: './turn-clock.component.html',
  styleUrls: ['./turn-clock.component.css']
})
export class TurnClockComponent implements OnInit {

  // Temps de départ du tour (timestamp)
  @Input() start: number;
  // Temps de fin (calculé)
  end: number;

  // Temps restant dans le tour en ms
  millisecondsLeft: number;

  // Minutes restantes dans le tour (partie entière)
  minutesLeft: number;

  // Secondes restantes dans le tour sans la partie entière des minutes restantes
  secondsLeft: number;

  constructor() { }

  // A l'initialisation du composant, calcule la date de fin du tour et démarre l'horloge
  ngOnInit() {
    this.end = this.start + 120000;
    this.setTime();
    this.registerForUpdate();
  }

  // Actualise l'horloge
  registerForUpdate()
  {
      let that = this;
      setTimeout(function(){
        that.setTime();
        that.registerForUpdate();
      },1000);
  }

  // Si la date de départ du tour change on recalcule la date de fin du tour et les données du temps
  ngOnChanges()
  {
      this.end = this.start+120000;
      this.setTime();
  }

  // Calcule les données du temps
  setTime()
  {
    this.millisecondsLeft = this.end - Date.now();
    this.minutesLeft = Math.trunc(this.millisecondsLeft/60000);
    this.secondsLeft = Math.trunc((this.millisecondsLeft-this.minutesLeft*60000)/1000);
  }
}
