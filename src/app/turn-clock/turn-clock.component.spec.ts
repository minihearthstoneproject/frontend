import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TurnClockComponent } from './turn-clock.component';

describe('TurnClockComponent', () => {
  let component: TurnClockComponent;
  let fixture: ComponentFixture<TurnClockComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TurnClockComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TurnClockComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
